## Parallelsを使用する際の注意事項

Parallersをvagrant上で使用するには、専用のプラグインを導入する必要がある。virtualboxはデフォルトで使用可能。
```
[...]# vagrant plugin install vagrant-parallels
```

負荷テストは[ここを使う](http://qiita.com/nii_yan/items/d7d0ea949abeab13aea7)といろいろ便利らしい